package main

import (
	"wall-e/src/internal/business"
	dao "wall-e/src/internal/dao/account"
	pkg "wall-e/src/pkg/http"
	"wall-e/src/pkg/http/controller"
)

func main() {
	accountDAO := dao.MakeAccountMemory()
	deposiBusiness := business.MakeDepositBusiness(accountDAO)
	accountController := controller.MakeGinAccountController(deposiBusiness)
	server := pkg.MakeGinServer(accountController)
	server.Listen(":3000")
}
