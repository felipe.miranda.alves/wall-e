package controller

import (
	"net/http"
	"wall-e/src/internal/business"

	"github.com/gin-gonic/gin"
)

type AccountController[T interface{}] interface {
	Deposit(T)
}

func MakeGinAccountController(
	depositBusiness *business.DepositBusiness,
) AccountController[*gin.Context] {
	return &accountGinController{
		depositBusiness: depositBusiness,
	}
}

type accountGinController struct {
	depositBusiness *business.DepositBusiness
}

func (a *accountGinController) Deposit(c *gin.Context) {
	var depositRequest struct {
		AccountID string  `json:"accountID"`
		Value     float64 `json:"value"`
	}
	if err := c.BindJSON(&depositRequest); err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	account, err := a.depositBusiness.Execute(depositRequest.AccountID, depositRequest.Value)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, account)
}
