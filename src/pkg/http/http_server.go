package pkg

import (
	"wall-e/src/pkg/http/controller"

	"github.com/gin-gonic/gin"
)

type HttpServer interface {
	Listen(port string) error
}

type HttpServerGin struct {
	gin      *gin.Engine
	basePath string
}

func MakeGinServer(accountController controller.AccountController[*gin.Context]) HttpServer {
	server := &HttpServerGin{
		gin: gin.Default(),
	}

	v1 := server.gin.Group("v1")
	{
		account := v1.Group("account")
		{
			account.POST("deposit", accountController.Deposit)
		}
	}

	return server
}

func (h *HttpServerGin) Listen(port string) error {
	return h.gin.Run(port)
}
