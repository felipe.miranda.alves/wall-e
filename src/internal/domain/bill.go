package domain

import "time"

type Bill struct {
	Id      string
	Value   float64
	DueDate time.Time
	PayDay  time.Time
	client  *client
}
