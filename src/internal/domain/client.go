package domain

type client struct {
	Id       string
	Name     string
	Document string
}

func MakeClient(id string, name string, document string) *client {
	return &client{
		Id:       id,
		Name:     name,
		Document: document,
	}
}
