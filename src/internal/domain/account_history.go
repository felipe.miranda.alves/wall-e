package domain

import "time"

type Operation string

const (
	Credit Operation = "credit"
	Debit            = "debit"
)

type AccountHistory struct {
	Id                string
	Type              Operation
	Origin            Account
	Destination       Account
	Value             float64
	DateTimeOperation time.Time
}
