package domain

import (
	"errors"
)

type Account interface {
	GetId() string
	Transfer(destination Account, value float64) error
	Deposit(value float64) (float64, error)
	Withdraw(value float64) (float64, error)
	GetBalance() float64
}

type accountImp struct {
	Id      string
	Client  *client
	Balance float64 `json:"balance"`
}

func MakeAccount(id string, client *client) Account {
	account := accountImp{
		Id:      id,
		Client:  client,
		Balance: 0,
	}
	return &account
}

func (a *accountImp) Transfer(destination Account, value float64) error {
	if a.Id == destination.GetId() {
		return errors.New("The Destination account cannot be the same as the origin account")
	}
	if value <= 0 {
		return errors.New("The transfer value must be greater than zero")
	}
	if _, err := a.Withdraw(value); err != nil {
		return err
	}
	destination.Deposit(value)
	return nil
}
func (a *accountImp) GetId() string {
	return a.Id
}

func (a *accountImp) Deposit(value float64) (float64, error) {
	if value <= 0 {
		return a.Balance, errors.New("The deposit value must be grater than zero")
	}
	a.Balance += value
	return a.Balance, nil
}

func (a *accountImp) Withdraw(value float64) (float64, error) {
	var newBalance float64
	if newBalance = a.Balance - value; newBalance < 0 {
		return a.Balance, errors.New("Insufficient balance for this operation")
	}
	a.Balance = newBalance
	return a.Balance, nil
}

func (a *accountImp) GetBalance() float64 {
	return a.Balance
}
