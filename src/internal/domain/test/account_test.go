package domaing_test

import (
	"errors"
	"testing"
	"wall-e/src/internal/domain"

	"github.com/stretchr/testify/assert"
)

var (
	mockClient = domain.MakeClient(
		"1",
		"Nome Teste",
		"12345678910",
	)
	mockAccount domain.Account
)

func beforEachTest(t *testing.T) {
	mockAccount = domain.MakeAccount("1", mockClient)
}

func Test_Make_Deposit_To_Account(t *testing.T) {
	beforEachTest(t)

	resultBalance, err := mockAccount.Deposit(1000)

	assert.Equal(t, resultBalance, float64(1000))
	assert.Nil(t, err)
}

func Test_Make_Withdraw_of_Account_With_Positive_Balance(t *testing.T) {
	beforEachTest(t)

	mockAccount.Deposit(1000)
	balance, err := mockAccount.Withdraw(1000)
	assert.Nil(t, err)
	assert.Equal(t, balance, float64(0))
}

func Test_Make_A_Withdrawal_Of_An_Amount_Above_The_Balance(t *testing.T) {
	beforEachTest(t)

	mockAccount.Deposit(1000)
	balance, err := mockAccount.Withdraw(3000)

	expectedError := errors.New("Insufficient balance for this operation")
	if assert.Error(t, err) {
		assert.Equal(t, expectedError, err)
		assert.Equal(t, balance, float64(1000))
	}
}

func Test_Make_A_Success_Transfer(t *testing.T) {
	beforEachTest(t)

	originAccount := mockAccount
	originAccount.Deposit(1000)

	destinationAccount := domain.MakeAccount("2", mockClient)
	err := originAccount.Transfer(destinationAccount, 1000)

	assert.Nil(t, err)
	assert.Equal(t, originAccount.GetBalance(), float64(0))
	assert.Equal(t, destinationAccount.GetBalance(), float64(1000))
}

func Test_Make_A_Transfer_With_An_Amount_Above_The_Balance(t *testing.T) {
	beforEachTest(t)

	originAccount := mockAccount
	originAccount.Deposit(1000)
	destinationAccount := domain.MakeAccount("2", mockClient)
	err := originAccount.Transfer(destinationAccount, 1500)

	expectedError := errors.New("Insufficient balance for this operation")

	if assert.Error(t, err) {
		assert.Equal(t, expectedError, err)
		assert.Equal(t, originAccount.GetBalance(), float64(1000))
		assert.Equal(t, destinationAccount.GetBalance(), float64(0))
	}
}

func Test_Make_A_Transfer_To_The_Same_Account(t *testing.T) {
	beforEachTest(t)

	originAccount := mockAccount
	originAccount.Deposit(1000)
	err := originAccount.Transfer(originAccount, 500)

	expectedError := errors.New("The Destination account cannot be the same as the origin account")

	if assert.Error(t, err) {
		assert.Equal(t, expectedError, err)
	}
}

func Test_Make_A_Transfer_With_zero_or_negative_value(t *testing.T) {
	beforEachTest(t)

	originAccount := mockAccount
	originAccount.Deposit(1000)
	destinationAccount := domain.MakeAccount("2", mockClient)
	err := originAccount.Transfer(destinationAccount, -1)

	expectedError := errors.New("The transfer value must be greater than zero")

	if assert.Error(t, err) {
		assert.Equal(t, expectedError, err)
	}
}
