package business

import (
	dao "wall-e/src/internal/dao/account"
	"wall-e/src/internal/domain"
)

func MakeBalanceGusiness(accountDAO dao.AccountDAO[domain.Account]) *BalanceBusiness {
	return &BalanceBusiness{accountDAO: accountDAO}
}

type BalanceBusiness struct {
	accountDAO dao.AccountDAO[domain.Account]
}

func (this *BalanceBusiness) GetBalance(accountID string) (domain.Account, error) {
	account, err := this.accountDAO.GetByID(accountID)
	if err != nil {
		return nil, err
	}
	return account, nil
}
