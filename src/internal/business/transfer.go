package business

import (
	dao "wall-e/src/internal/dao/account"
	"wall-e/src/internal/domain"
)

func MakeTransferBusiness(accountDAO dao.AccountDAO[domain.Account]) *TransferBusiness {
	return &TransferBusiness{accountDAO: accountDAO}
}

type TransferBusiness struct {
	accountDAO dao.AccountDAO[domain.Account]
}

func (this *TransferBusiness) Execute(originAccountID string, destinationAccountID string, value float64) (domain.Account, error) {

	originAccount, err := this.accountDAO.GetByID(originAccountID)
	if err != nil {
		return nil, err
	}

	destinationAccount, err := this.accountDAO.GetByID(destinationAccountID)
	if err != nil {
		return nil, err
	}

	trnasferErr := destinationAccount.Transfer(originAccount, value)
	if trnasferErr != nil {
		return nil, trnasferErr
	}

	return originAccount, nil
}
