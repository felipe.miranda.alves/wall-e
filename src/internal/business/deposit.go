package business

import (
	dao "wall-e/src/internal/dao/account"
	"wall-e/src/internal/domain"
)

func MakeDepositBusiness(accountDAO dao.AccountDAO[domain.Account]) *DepositBusiness {
	return &DepositBusiness{accountDAO: accountDAO}
}

type DepositBusiness struct {
	accountDAO dao.AccountDAO[domain.Account]
}

func (this *DepositBusiness) Execute(accountID string, value float64) (domain.Account, error) {
	account, err := this.accountDAO.GetByID(accountID)
	if err != nil {
		return nil, err
	}

	_, depositErr := account.Deposit(value)
	if depositErr != nil {
		return nil, depositErr
	}

	return account, nil
}
