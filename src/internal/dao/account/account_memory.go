package dao

import (
	"errors"
	"wall-e/src/internal/domain"
)

func MakeAccountMemory() AccountDAO[domain.Account] {
	accounts := []domain.Account{
		domain.MakeAccount("1", nil),
	}
	return &AccountDAOMemory{
		accounts: accounts,
	}
}

type AccountDAOMemory struct {
	accounts []domain.Account
}

func (a *AccountDAOMemory) GetByID(ID string) (account domain.Account, err error) {
	for i := range a.accounts {
		if a.accounts[i].GetId() == ID {
			account = a.accounts[i]
			break
		}
	}
	if account == nil {
		err = errors.New("Account not found")
	}
	return
}

func (a *AccountDAOMemory) Create(account domain.Account) (domain.Account, error) {
	return nil, nil
}
func (a *AccountDAOMemory) Update(account domain.Account) (domain.Account, error) {
	return nil, nil
}
