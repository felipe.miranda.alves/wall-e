package dao

import "wall-e/src/internal/domain"

type AccountDAO[T domain.Account] interface {
	GetByID(ID string) (T, error)
	Create(t T) (T, error)
	Update(t T) (T, error)
}
