# wall-e

# Arquitetura
A arquitetura proposta diferencia um client de outro por tenantID paassado por parâmetro para o API Gateway.

No caso de clientes mobile, pode ser utilizado websocket para ter um canal de stream onde o Gateway fará a mediação entre a conexão no cliente websocket com as chamadas http dos microserviços.

Ainda no caso dos mobiles, é possível criar um único endpoint para realizar operações onde seriam necessárias várias requisições e agrupá-las num único retorn para diminir o overhead por aberturas de conexões http (se for o caso).

Somete o micro serviço Transfer está representando iteração com o redis, mas considere que todos irão fazer a mesma coisa.

Todos os micro serviços foram pensados para funcionar isoladamente porém seria facilmente agrupado ou serparado para aumentar ou diminir sua responsabilidade. 

![](https://gitlab.com/felipe.miranda.alves/wall-e/-/raw/1a908980b532cfde7e4d6f3c3c151975527d7ccf/arch.png?raw=true)

# Docker
Cada micro serviço teria um binário separadamente e uma imagem docker seria gerado para que, quando fosse feito o deploy, cara imagem seria utilizada em seu contexto.

A idéia era disponibilizar todo o ambiente dev em um ambiente docker-compose para facilitar a subida dos serviços necessários

